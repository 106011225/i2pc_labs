#include <assert.h>
#include <stdio.h>
#include <math.h>
#include <mpi.h>

unsigned long long r, k;

void add_and_mod(void* inVec_t, void* inoutVec_t, int *len, MPI_Datatype *dptr){
    unsigned long long* inVec = (unsigned long long*)inVec_t;
    unsigned long long* inoutVec = (unsigned long long*)inoutVec_t;
    for (int i=0; i < *len; i++){
        *inoutVec = (*inoutVec + *inVec) % k;
        inVec++; inoutVec++;
    }
}


int main(int argc, char** argv) {
	if (argc != 3) {
		fprintf(stderr, "must provide exactly 2 arguments!\n");
		return 1;
	}
	r = atoll(argv[1]);
	k = atoll(argv[2]);





    MPI_Init(&argc, &argv);

    int rank, world_size;
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &world_size);

    // calculate cutPoints
    // unsigned long long beginP = ceil(r * cbrtl( ((long double)rank)/world_size ));
    // unsigned long long endP = ceil(r * cbrtl( ((long double)(rank+1))/world_size ));
    unsigned long long beginP = ceil(r * ((double)rank / world_size));
    unsigned long long endP = ceil(r * ((double)(rank+1) / world_size));


    // calculate patial sum
	unsigned long long pixels = 0;
    unsigned long long total_pixels;
	for (unsigned long long x = beginP; x < endP; x++) {
		unsigned long long y = ceil(sqrtl(r*r - x*x));
		pixels += y;
		pixels %= k;
	}
    // printf("process %d done.\n", rank);


    MPI_Barrier(MPI_COMM_WORLD);

    MPI_Op myReduceOp;
    MPI_Op_create(add_and_mod, 1, &myReduceOp);
    MPI_Reduce(&pixels, &total_pixels, 1, MPI_UNSIGNED_LONG_LONG, myReduceOp, 0, MPI_COMM_WORLD);
    if (rank == 0) printf("%llu\n", (4 * total_pixels) % k);
    


    // if (rank != 0){
    //     MPI_Send(&pixels, 1, MPI_UNSIGNED_LONG_LONG, 0, 0, MPI_COMM_WORLD);
    // }
    // else {
    //     total_pixels = pixels;
    //     for (int i=1; i < world_size; i++){
    //         MPI_Recv(&pixels, 1, MPI_UNSIGNED_LONG_LONG, i, 0, MPI_COMM_WORLD, MPI_STATUSES_IGNORE);
    //         total_pixels = (total_pixels + pixels) % k;
    //     }
    //     printf("%llu\n", (4 * total_pixels) % k);
    // }

    MPI_Finalize();
    return 0;
}








// typedef struct { 
//     double real,imag; 
// } Complex; 
 
/* the user-defined function 
 */ 
// void myProd( Complex *in, Complex *inout, int *len, MPI_Datatype *dptr ) 
// { 
//     int i; 
//     Complex c; 

 
//     for (i=0; i< *len; ++i) { 
//             c.real = inout->real*in->real - 
//                        inout->imag*in->imag; 
//             c.imag = inout->real*in->imag + 
//                        inout->imag*in->real; 
//             *inout = c; 
//             in++; inout++; 
//     } 
// } 

 
/* and, to call it... 
 */ 
// ... 

 
/* each process has an array of 100 Complexes 
     */ 
//     Complex a[100], answer[100]; 
//     MPI_Op myOp; 
//     MPI_Datatype ctype; 

 
/* explain to MPI how type Complex is defined 
     */ 
//     MPI_Type_contiguous( 2, MPI_DOUBLE, &ctype ); 
//     MPI_Type_commit( &ctype ); 
    /* create the complex-product user-op 
     */ 
//     MPI_Op_create( myProd, True, &myOp ); 

 
// MPI_Reduce( a, answer, 100, ctype, myOp, root, comm ); 
