# Use the 'File' menu above to 'Save' after pasting in your 3 function calls.
%%timeit
# Feel free to modify the 3 function calls in this cell
normalize(greyscales_dev, out=normalized_dev)
weigh(normalized_dev, weights_dev, out=weighted_dev)
activate(weighted_dev, out=activated_dev)
SOLUTION = activated_dev.copy_to_host()