# Use the 'File' menu above to 'Save' after pasting in your imports, data, and function definitions.
# Remember that we can't use numpy math function on the GPU...
from numpy import exp
import math
import numba

greyscales_dev = numba.cuda.to_device(greyscales)
weights_dev = numba.cuda.to_device(weights)

# Consider modifying the 3 values in this cell to optimize host <-> device memory movement
#normalized = np.empty_like(greyscales)
normalized_dev = numba.cuda.device_array(shape=(n,), dtype=np.float32)
#weighted = np.empty_like(greyscales)
weighted_dev = numba.cuda.device_array(shape=(n,), dtype=np.float32)
#activated = np.empty_like(greyscales)
activated_dev = numba.cuda.device_array(shape=(n,), dtype=np.float32)

# Modify these 3 function calls to run on the GPU
@numba.vectorize(['float32(float32)'], target='cuda')
def normalize(grayscales):
    return grayscales / 255

@numba.vectorize(['float32(float32, float32)'], target='cuda')
def weigh(values, weights):
    return values * weights

@numba.vectorize(['float32(float32)'], target='cuda')
def activate(values):
    #return ( np.exp(values) - np.exp(-values) ) / ( np.exp(values) + np.exp(-values) )
    return ( math.exp(values) - math.exp(-values) ) / ( math.exp(values) + math.exp(-values) )