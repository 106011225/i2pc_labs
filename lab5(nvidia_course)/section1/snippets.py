from numba import jit
from numba import vectorize
from numba import cuda
import math
import numpy as np

@jit

@vectorize


@vectorize(['float32(float32, float32)'], target='cuda')
@cuda.jit(device=True)


n = 100000
X = np.arange(n).astype(np.float32)
X_dev = cuda.to_device(X)

out_dev = cuda.device_array(shape=(n,), dtype=np.float32)
out_dev = cuda.device_array_like(X_dev)

out_host = out_dev.copy_to_host()




@cuda.jit
def add_kernel(x, y, out):
    start = cuda.grid(1)
    stride = cuda.gridsize(1)   
    for i in range(start, x.shape[0], stride):
        out[i] = x[i] + y[i]

threads_per_block = 128
blocks_per_grid = 32
add_kernel[blocks_per_grid, threads_per_block](d_x, d_y, d_out)
cuda.synchronize()



# Safely add 1 to offset 0 in global_counter array
cuda.atomic.add(global_counter, 0, 1)  




A = np.zeros(16).reshape(4, 4).astype(np.int32)
d_A = cuda.to_device(A)

@cuda.jit
def add_2D_coordinates_stride(A):
    grid_y, grid_x = cuda.grid(2)
    stride_y, stride_x = cuda.gridsize(2)
    for data_i in range(grid_x, A.shape[0], stride_x):
        for data_j in range(grid_y, A.shape[1], stride_y):
            A[data_i][data_j] = grid_x + grid_y

blocks = (2, 2)
threads_per_block = (2, 2)
add_2D_coordinates[blocks, threads_per_block](d_A)
print(d_A.copy_to_host())


from numba import types, cuda
@cuda.jit
def swap_with_shared(x, y):
    # Allocate a 4 element vector containing int32 values in shared memory.
    temp = cuda.shared.array(4, dtype=types.int32)
    idx = cuda.grid(1)
    temp[idx] = x[idx]
    cuda.syncthreads()
    y[idx] = temp[cuda.blockDim.x - cuda.threadIdx.x - 1] # swap elements




@cuda.jit
def tile_transpose(a_in, a_out):
    # THIS CODE ASSUMES IT IS RUNNING WITH A BLOCK DIMENSION OF (TILE_SIZE x TILE_SIZE)
    # AND INPUT IS A MULTIPLE OF TILE_SIZE DIMENSIONS
    tile = cuda.shared.array((TILE_DIM, TILE_DIM), numba.types.int32)
    x = cuda.blockIdx.x * TILE_DIM + cuda.threadIdx.x
    y = cuda.blockIdx.y * TILE_DIM + cuda.threadIdx.y
    for j in range(0, TILE_DIM, BLOCK_ROWS):
        tile[cuda.threadIdx.y + j, cuda.threadIdx.x] = a_in[y + j, x]
    cuda.syncthreads()  
    x = cuda.blockIdx.y * TILE_DIM + cuda.threadIdx.x
    y = cuda.blockIdx.x * TILE_DIM + cuda.threadIdx.y
    for j in range(0, TILE_DIM, BLOCK_ROWS):
        a_out[y + j, x] = tile[cuda.threadIdx.x, cuda.threadIdx.y + j];

tile_transpose[grid_shape,(TILE_DIM, BLOCK_ROWS)](a_in, a_out)






# Leave the values in this cell alone
M = 128
N = 32

# Input vectors of MxN and NxM dimensions
a = np.arange(M*N).reshape(M,N).astype(np.int32)
b = np.arange(M*N).reshape(N,M).astype(np.int32)
c = np.zeros((M, M)).astype(np.int32)

d_a = cuda.to_device(a)
d_b = cuda.to_device(b)
d_c = cuda.to_device(c)

# NxN threads per block, in 2 dimensions
block_size = (N,N)
# MxM/NxN blocks per grid, in 2 dimensions
grid_size = (int(M/N),int(M/N))



import numpy as np
from numba import cuda, types
@cuda.jit
def mm_shared(a, b, c):
    column, row = cuda.grid(2)
    sum = 0

    # `a_cache` and `b_cache` are already correctly defined
    a_cache = cuda.shared.array(block_size, types.int32)
    b_cache = cuda.shared.array(block_size, types.int32)

    # TODO: use each thread to populate one element each a_cache and b_cache
    
    a_cache[cuda.threadIdx.y][cuda.threadIdx.x] = a[row][cuda.threadIdx.x]
    b_cache[cuda.threadIdx.y][cuda.threadIdx.x] = b[cuda.threadIdx.y][column]
    cuda.syncthreads()
    
    # TODO: calculate the `sum` value correctly using values from the cache 

    for k in range(block_size[1]):
        sum += a_cache[cuda.threadIdx.y][k] * b_cache[k][cuda.threadIdx.x]
        
    c[row][column] = sum



# There's no need to update this kernel launch
mm_shared[grid_size, block_size](d_a, d_b, d_c)



# Do not modify the contents in this cell
from numpy import testing
solution = a@b
output = d_c.copy_to_host()
# This assertion will fail until you correctly update the kernel above.
testing.assert_array_equal(output, solution)
