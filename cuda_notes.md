_ _ _
# General Concepts

- kernel cannot have **static variables**
- fully utilize all resources  
    - resources of all channels, computing nodes/devices (CPUs, GPUs)  
    - resources of all SMs  
    - resources of an SM  
- Load balance   
- Control Flow
    - instructions are issued per 32 threads (warp)
    - Different warps can execute different code with no impact on performance
    - Avoid diverging within a warp
        - Example with divergence:
            - `if (threadIdx.x > 2) {...} else {...}`
        - Example without divergence:
            - `if (threadIdx.x / WARP_SIZE > 2) {...} else {...}`  
- Launch Configuration
    - Latency is hidden by switching threads. Need enough threads to hide latency.  
    - Hiding arithmetic latency:
        - Need ~18 warps (576) threads per SM
        - Or, latency can also be hidden with independent instructions from the same warp
            - For example, if instruction never depends on the output of preceding instruction, then only 9 warps are needed, etc.

- set threadsPerBlock to  multiple of warp size (32)  
- Each **SM** should have enough **active warps** to sufficiently hide all of the **memory** and **instruction pipeline latency** to achieve maximum throughput  
    - repeated sumulations and experiments help to identify the sweet spot   
    - Goal: To maximize GPU Occupancy  
        - Occupancy = Active Warps / Maximum Allowable Active Warps (Maximum number of resident warps per multiprocessor)
    - Resources are allocated for the entire block  
        - Resources are finite  
        - Utilizing too many resources per thread may limit the occupancy  
            - Registers  
            - Shared memory usage  
            - Block size  



_ _ _
# Device Management
- Application can query and select GPUs
    - `cudaGetDeviceCount(int *count)`
    - `cudaSetDevice(int device)`
    - `cudaGetDevice(int *device)`
    - `cudaGetDeviceProperties(cudaDeviceProp *prop, int device)`
- Multiple cpu threads can share a device
- A single cpu thread can manage multiple devices
    - `cudaSetDevice(i)` to select current device
    - `cudaMemcpy(…)` for peer-to-peer copies 



_ _ _
# Asynchronous Execution

- Overlap CPU and kernel execution
- Overlap data transfers and kernel execution (pipeline)
    - non-default stream
    - **page-locked** host memory

```c++
int N = 2<<24;
int size = N * sizeof(int);

int *host_array;
int *device_array;

cudaMallocHost(&host_array, size);               // Pinned host memory allocation.
cudaMalloc(&device_array, size);                 // Allocation directly on the active GPU device.

initializeData(host_array, N);                   // Assume this application needs to initialize on the host.

const int numberOfSegments = 4;                  // This example demonstrates slicing the work into 4 segments.
int segmentN = N / numberOfSegments;             // A value for a segment's worth of `N` is needed.
size_t segmentSize = size / numberOfSegments;    // A value for a segment's worth of `size` is needed.

// For each of the 4 segments...
for (int i = 0; i < numberOfSegments; ++i)
{
  // Calculate the index where this particular segment should operate within the larger arrays.
  segmentOffset = i * segmentN;

  // Create a stream for this segment's worth of copy and work.
  cudaStream_t stream;
  cudaStreamCreate(&stream);

  // Asynchronously copy segment's worth of pinned host memory to device over non-default stream.
  cudaMemcpyAsync(&device_array[segmentOffset],  // Take care to access correct location in array.
                  &host_array[segmentOffset],    // Take care to access correct location in array.
                  segmentSize,                   // Only copy a segment's worth of memory.
                  cudaMemcpyHostToDevice,
                  stream);                       // Provide optional argument for non-default stream.

  // Execute segment's worth of work over same non-default stream as memory copy.
  kernel<<<number_of_blocks, threads_per_block, 0, stream>>>(&device_array[segmentOffset], segmentN);

  // `cudaStreamDestroy` will return immediately (is non-blocking), but will not actually destroy stream until
  // all stream operations are complete.
  cudaStreamDestroy(stream);
}
```



_ _ _
# Synchonization

Atomic operations
- `float atomicAdd(float* addr, float amount)`
    - Adds the value of `*addr` and `amount`, and stores the result back to `addr`, return the old value of `*addr`
    - Atomic operations on both **global** and **shared** memory variables
    - `atomicSub()`, `atomicMin()`, `atomicMax()`, `atomicExch()`, etc.






_ _ _
# Memory

## Memory Characteristic

Global, local, and texture memory have the greatest access latency, followed by constant memory, shared memory, and the register file.

Per-block shared memory: Readable/writable by all threads in block  
Per-thread private memory: Readable/writable by a thread  
Device global memory: Readable/writable by all threads  


- `__device__` (Device global memory)
    - Resides in global memory space
    - Has the lifetime of an application
    - Is acessible from all the threads within the grid
- `__constant__` (Device global memory)
    - Resides in constant memory space
    - Has the lifetime of an application
    - Is acessible from all the threads within the grid
- `__shared__` (Shared memory, L1 cache level memory)
    - Resides in the shared memory space of a thread block. Access is about 100x faster than global memory access
    - Has the lifetime of the block
    - Is only accessible from all the threads within the block
    - `__syncthreads()`: Barrier, wait for all threads in a block to arrive at this point 


Page-Locked Host Memory  
- `cudaMallocHost()`, `cudaFreeHost()`


Unified Memory
- If the access patttern of data is not predictable or is sparse, then on-demand access is preferable
- `cudaMallocanaged()`, `cudaFree()`
- `cudaMemPrefetchAsync()`


Constant Memory  
- There is a total of 64 KB constant memory on a device. The constant memory space is cached. As a result, a read from constant memory costs one memory read from device memory only on a cache miss; otherwise, it just costs one read from the constant cache. **Accesses to different addresses by threads within a warp are serialized, thus the cost scales linearly with the number of unique addresses read by all threads within a warp.** As such, the constant cache is best when threads in the same warp accesses only a few distinct locations. If all threads of a warp access the same location, then constant memory can be as fast as a register access.


## Memory Optimization 

- Memory Access Coalesce  
    1. Global memory requests of the threads in a warp are "grouped" as much as possible  
    2. Global memory are aligned in units of 128 Bytes. Access across the boundary will cause multiple memory accesses.  
    3. Loaded from global memory to cache in units of 32, 64, or 128 Bytes  
    4. Threads load data from the cache  


- Shared memory access
    - shared memory is split into memory banks
    - each memory bank can process one memory access at a time
    - access the same bank by different threads lead to conflicts. requests are serialized if requests to the same bank occur
    - when all threads in a warp access the same bank, word is broadcast to all threads (no confilct occurs)  
    - maximize the usage of all banks  
    [fig. link](https://docs.nvidia.com/cuda/cuda-c-programming-guide/index.html#shared-memory-5-x)
    >On devices of compute capability 5.x or newer, each bank has a bandwidth of 32 bits every clock cycle, and **successive 32-bit words are assigned to successive banks**. The warp size is 32 threads and the number of banks is also 32, so bank conflicts can occur between any threads in the warp.  
    >A shared memory request for a warp does not generate a bank conflict between two threads that access any address within the same 32-bit word (even though the two addresses fall in the same bank). In that case, for read accesses, the word is broadcast to the requesting threads and for write accesses, each address is written by only one of the threads (which thread performs the write is undefined).


- False sharing  
    - Condition where two processors write to different addresses, but addresses map to the same cache line  
    - False sharing can affect performance when programming for cache-coherent architectures  
    - cache line size: 128 Bytes  



- GMEM Operation
    - Two types of loads
        - Caching
            - Load granularity is 128-Byte line
                - If warp requests 32 misaligned, consecutive 4-Byte words, addr fall within 2 cache-lines (256 Bytes)
                - If all threads in a warp request the same 4-Byte word, addr fall within a single cache-line (128 Bytes)
        - Non-caching
            - Attempts to hit in L2, then GMEM; do not hit in L1
            - Load granularity is 32-bytes
                - If warp requests 32 misaligned, consecutive 4-Byte words, addr fall within at most 5 segments (160 Bytes)
                - If all threads in a warp request the same 4-Byte word, addr fall within a single segment (32 Bytes)
    - Optimization guidelines
        - Strive for perfect coalescing
            - Align starting address (may require padding)
            - A warp should access within a contiguous region
        - Have enough concurrent accesses to saturate the bus
            - Process several elements per thread
                - Multiple loads get pipelined
                - Indexing calculations can often be reused
            - Launch enough threads to maximize throughput
                - Latency is hidden by switching threads (warps)
        - Try L1 and caching configurations to see which one works best
            - Caching vs non-caching loads (compiler option)
            - 16KB vs 48KB L1 (CUDA call)


- Local Memory and Register Spilling
    - [Ref](https://developer.download.nvidia.com/CUDA/training/register_spilling.pdf)
    - Local memory
        - Name refers to memory where registers and other thread-data is spilled
        - bytes are stored in global memory
        - Addressing is resolved by the compiler
        - Stores are cached in L1
    - LMEM Access Operation
        - A store/load writes/requests a line to/from L1; evicted/miss -> L2; evicted/miss -> DRAM
    - When is Local Memory Used?
        - Register spilling
        - Arrays declared inside kernels, if compiler cannot resolve indexing
    - How Does LMEM Affect Performance?
        - It could hurt performance in two ways
            - Increased memory traffic
            - Increased instruction count
        - Spilling/LMEM usage isn’t always bad
            - LMEM bytes can get contained within L1
                - Avoids memory traffic increase
            - Additional instructions don’t matter much if code is not instruction-throughput limited
    - General Analysis/Optimization Steps
        - Check for LMEM usage
            - Compiler output
                - nvcc option: `-Xptxas -v,-abi=no`
                - Will print the number of lmem bytes for each kernel (only if kernel uses LMEM)
            - Profiler
        - Check the impact of LMEM on performance
            - Bandwidth-limited code:
                - Check how much of L2 or DRAM traffic is due to LMEM
            - Arithmetic-limited code:
                - Check what fraction of instructions issued is due to LMEM
        - Optimize:
            - Try: increasing register count, increasing L1 size, using non-caching loads
    - Register Spilling: Analysis
        - Profiler counters:
            - `l1_local_load_hit`, `l1_local_load_miss`, `l1_local_store_hit`, `l1_local_store_miss`
            - Counted for a single SM, incremented by 1 for each 128-byte transaction
        - Impact on memory
            - Any memory traffic that leaves SMs (goes to L2) is expensive
            - L2 counters of interest: read and write sector queries
                - Actual names are longer, check the profiler documentation
                - Incremented by 1 for each 32-byte transaction
            - Compare:
                - Estimated L2 transactions due to LMEM misses in all the SMs
                    - 2*(number of SMs)*4*l1_local_load_miss
                        - 2: load miss implies a store happened first
                        - Number of SMs: `l1_local_load_miss` counter is for a single SM
                        - 4: local memory transaction is 128-bytes = 4 L2-transactions
                - Sum of L2 read and write queries (not misses)
        - Impact on instructions
            - Compare the sum of all LMEM instructions to total instructions issued
    - Optimizations When Register Spilling is Problematic
        - Try increasing the limit of registers per thread
            - Use a higher limit in `-maxrregcount`, or lower thread count for `__launch_bounds__`
            - Likely reduces occupancy, potentially reducing execution efficiency
                - may still be an overall win - fewer total bytes being accessed
        - Try using non-caching loads for global memory
            - nvcc option: `-Xptxas -dlcm=cg`
            - Potentially fewer contentions with spilled registers in L1
        - Increase L1 size to 48KB
            - Default is 16KB L1, larger L1 increases the chances for LMEM hits
            - Can be done per kernel or per device:
                - `cudaFuncSetCacheConfig()`, `cudaDeviceSetCacheConfig()`
                - `cudaDeviceSetCacheConfig(cudaFuncCachePreferL1);`
    - Examples
        - `$ nvcc -arch=sm_20 -Xptxas -v,-abi=no,-dlcm=cg fwd_o8.cu -maxrregcount=32`





_ _ _
# Others GPU techniques

- Unrolling: avoid branch divergence; Avoid using array  
    - `#pragma unroll`
    - Avoid using array. Only **static indexed** array can be converted to register during compiling. Otherwise, data will be stored in **local memory** (in global memory).
    ```c++
    int x[4];
    #pragma unroll
    for (int i = 0; i < 4; i++)
        x[i] = i;

    /************************/

    int x[4];
    // Manually unroll
    x[0] = 0;
    x[1] = 1;
    x[2] = 2;
    x[3] = 3;
    ```

- Runtime Math Library and Intrinsics
    - Two types of runtime math library functions
        - __func(): many map directly to hardware ISA
        - Fast but lower accuracy (see CUDA Programming Guide for full details)
        - Examples: `__sinf(x)`, `__expf(x)`, `__powf(x, y)`
        - func(): compile to multiple instructions
        - Slower but higher accuracy (5 ulp or less)
        - Examples: `sin(x)`, `exp(x)`, `pow(x, y)`
    - A number of additional intrinsics:
        - `__sincosf()`, `__frcp_rz()`, ...
        - Explicit IEEE rounding modes (rz,rn,ru,rd)

- Random shuffle

- Error Handling
    ```c++
    #include <stdio.h>
    #include <assert.h>

    inline cudaError_t checkCuda(cudaError_t result)
    {
      if (result != cudaSuccess) {
        fprintf(stderr, "CUDA Runtime Error: %s\n", cudaGetErrorString(result));
        assert(result == cudaSuccess);
      }
      return result;
    }
    ```

    ```c++
    someKernel<<<1, -1>>>();
    cudaError_t err;
    err = cudaGetLastError();
    if (err != cudaSuccess) {
      printf("Error: %s\n", cudaGetErrorString(err)); 
    }
    ```

_ _ _
# Analysis-Driven Optimization


- Analysis with Profiling cmd
    - `$ nvprof <executable>`  
    - `$ nsys profile --stats=true <executable>`  
    - `$ nsys-ui`  
    - `$ prime-run nsys-ui`
- Analysis with Modified Source Code
    - Time memory-only and math-only versions of the kernel
    - Then, compare times for modified kernels
        - Helps decide whether the kernel is mem or math bound
        - Shows how well memory operations are overlapped with arithmetic

## Performance Optimization Process

- Determine the limits for kernel performance
    - Memory throughput
    - Instruction throughput
    - Latency
    - Combination of the above
- Use appropriate performance metric for each kernel
    - For example, for a memory bandwidth-bound kernel, Gflops/s don't make sense
- Address the limiters in the order of importance
    - Determine how close resource usage is to the HW limits
    - Analyze for possible inefficiencies
    - Apply optimizations
        - Often these will just be obvious from how HW operate


Source Modification
- Memory-only: Remove as much arithmetic as possible
- Store-only: Also remove the loads
- Math-only: Remove global memory accesses
    - Need to trick the compiler:
        - Compiler throws away all code that it detects as not contributing to gmem stores
        - Put gmem stores inside conditionals that always evaluate to **false**
            - Condition outcome should not be known to the compiler (kernel parameter)
- Note that Removing pieces of code is likely to affect register count
    - Make sure to keep the same occupancy
        - if necessary add dummy shared memory `kernel<<<grid, block, smem, ...>>>(...)`

    - Case Study: Limiter Analysis
        ```
        - Time (ms)
            - Full-kernel: 35.39
            - Mem-only:    33.27
            - Math-only:   16.25
        - Instructions issued
            - Full-kernel: 18194139
            - Mem-only:     7497269
            - Math-only:   16839792
        - Memory access transactions
            - Full-kernel: 1708032 
            - Mem-only:    1708032 
            - Math-only:         0
        ```
        - Analysis  
            - Instruction:Byte ratio = `(32*18194139)/(128*1708032)` = ~2.66
            - Good overlap between math and mem
                - 2.12ms of math only time (13%) are not overlapped with mem
            - App memory throughput 62 GB/s, while HW theory is 114 GB/s, so we're off optimum
        - Conclusion
            - Code is memory-bound
            - Latency could be an issue too
            - Optimizations should focus on memory throughput first
                - math contributes very little to toal time (2.12 out of 35.39ms)


## Optimizations for Global Memory (pending)
## Optimizations for Instruction Throughput (pending)
## Optimizations for Latency (pending)
## Register Spilling (pending)
## (pending)




_ _ _
# Optimizing Parallel Reduction in CUDA

- Tree-based approach
    - decompose into multiple kernels, of which code for all levels are the same
        - Resursive kernel invocation
- Reductions have very low arithmetic intensity. Therefore we should strive for peak bandwidth rather than GFLOP/s

## Reduction #1: Interleaved Addressing
```c++
__global__ void reduce0(int *g_idata, int *g_odata) {
    extern __shared__ int sdata[];
    // each thread loads one element from global to shared mem
    unsigned int tid = threadIdx.x;
    unsigned int i = blockIdx.x*blockDim.x + threadIdx.x;
    sdata[tid] = g_idata[i];
    __syncthreads();
    // do reduction in shared mem
    for(unsigned int s=1; s < blockDim.x; s *= 2) {
        if (tid % (2*s) == 0) { // [PROBLEM] highly devergent warps are very inefficient, and % is very slow
            sdata[tid] += sdata[tid + s];
        }
        __syncthreads();
    }
    // write result for this block to global mem
    if (tid == 0) g_odata[blockIdx.x] = sdata[0];
}
```
- working threadIdx.x
    - 0, 2, 4, 6, 8, 10, 12, 14
    - 0, 4, 8, 12
    - 0, 8
    - 0


## Reduction #2: Interleaved Addressing
Replace divergent branch in inner loop with strided index and non-divergent branch
```c++
for (unsigned int s=1; s < blockDim.x; s *= 2) {
    int index = 2 * s * tid;
    if (index < blockDim.x) {
        sdata[index] += sdata[index + s]; // [PROBLEM] Shared Memory Bank Conflicts
    }
    __syncthreads();
}
```
- working threadIdx.x
    - 0, 1, 2, 3, 4, 5, 6, 7
    - 0, 1, 2, 3
    - 0, 1
    - 0


## Reduction #3: Sequential Addressing
Replace strided indexing in inner loop with reversed loop and threadIdx-based indexing
```c++
for (unsigned int s=blockDim.x/2; s>0; s>>=1) {
    if (tid < s) { // [PROBLEM] Half of the threads are idle on first loop iteration, which is wasteful
        sdata[tid] += sdata[tid + s];
    }
    __syncthreads();
}
```

## Reduction #4: First Add During Load
Halve the numbre of blocks, and replace single load with 2 loads and first add of the reduction
```c++
// perform first level of reduction,
// reading from global memory, writing to shared memory
unsigned int tid = threadIdx.x;
unsigned int i = blockIdx.x*(blockDim.x*2) + threadIdx.x;
sdata[tid] = g_idata[i] + g_idata[i+blockDim.x];
__syncthreads();
```

## Reduction #5: Unroll the Last Warp
Unroll the last 6 iterations of the inner loop
    - When `s <= 32`, we have only ONE warp, SIMD
        - We don't need `__syncthreads()`
        - We don't need `if (tid < s)` because it doesn't save any work
```c++
for (unsigned int s=blockDim.x/2; s>32; s>>=1) {
    if (tid < s)
        sdata[tid] += sdata[tid + s];
    __syncthreads();
}
if (tid < 32) {
    sdata[tid] += sdata[tid + 32];// iteration [-6]
    sdata[tid] += sdata[tid + 16];// iteration [-5]
    sdata[tid] += sdata[tid + 8]; // iteration [-4]
    sdata[tid] += sdata[tid + 4]; // iteration [-3]
    sdata[tid] += sdata[tid + 2]; // iteration [-2]
    sdata[tid] += sdata[tid + 1]; // iteration [-1]
}
```

## Reduction #7: Multiple Adds / Thread
- Algorithm Cascading
    - Brent's theorem says each thread should reduce O(log n) elements
    - Possibly better latency hiding with more work per thread
Replace load and add of two elements with a while loop to add as many as necessary
```c++
unsigned int tid = threadIdx.x;
unsigned int i = blockIdx.x*(blockSize*2) + threadIdx.x;
unsigned int stride = (blockSize*2)*gridDim.x;
sdata[tid] = 0;
while (i < n) {
    sdata[tid] += g_idata[i] + g_idata[i+blockSize];
    i += stride; // `stride` loop stride to maintain coalescing
}
__syncthreads();
```


_ _ _
# Non-Gpu Optimization

## multithreads(MIMD)  

```c++
#pragma omp collapse(2)
for (int i = 0; i < a; i++)
    for (int j = 0; j < b; j++)
```

## vector instruction    
[Intel Intrinsics Guide](https://www.intel.com/content/www/us/en/docs/intrinsics-guide/index.html)  
`$ lscpu`  
sse sse2 ssse3 sse4_1 sse4_2  
- SSE2: 128-bit
    - Pack 4 32-bit floating points (ps, "packed" "single")
    - pack 2 64-bit floating points (pd)
```c++
__m128d _mm_load_pd (double const* mem_addr)
__m128d _mm_mul_pd (__m128d a, __m128d b)
void _mm_store_pd (double* mem_addr, __m128d a)

// Example: z[i] = x[i] * y[i]

double x[2], y[2], z[2]
__m128d a = _mm_load_pd(x);
__m128d b = _mm_load_pd(y);
a = _mm_mul_pd(a, b);
_mm_store_pd(z, a);
```



_ _ _
# SPEC

## sm_61  
|Technical specifications|value|
| --- | --- |
Maximum number of resident grids per device| 32  
Maximum dimensionality of grid of thread blocks| 3  
Maximum x-dimension of a grid of thread blocks| 2^31 - 1  
Maximum y-, or z-dimension of a grid of thread blocks| 65535  
Maximum dimensionality of thread block| 3  
Maximum x- or y-dimension of a block| 1024  
Maximum z-dimension of a block| 64  
Maximum number of threads per block| 1024  
Warp size| 32  
**Maximum number of resident blocks per multiprocessor**| 32  
**Maximum number of resident warps per multiprocessor**| 64  
**Maximum number of resident threads per multiprocessor**| 2048  
**Number of 32-bit registers per multiprocessor**| 64K  
Maximum number of 32-bit registers per thread block| 64K  
Maximum number of 32-bit registers per thread| 255  
**Maximum amount of shared memory per multiprocessor**| 96KB  
**Maximum amount of shared memory per thread block**| 48KB  
Number of shared memory banks| 32  
Amount of local memory per thread| 512KB  
Constant memory size| 64KB  
Cache working set per multiprocessor for constant memory| 8K  
Cache working set per multiprocessor for texture memory| 48KB  
Maximum width for 1D texture reference bound to a CUDA array| 131072	  
Maximum width for 1D texture reference bound to linear memory| 2^27  
Maximum width and number of layers for a 1D layered texture reference| 32768 x 2048  
Maximum width and height for 2D texture reference bound to a CUDA array| 131072 x 65536  
Maximum width and height for 2D texture reference bound to a linear memory| 131072 x 65000 	  
Maximum width and height for 2D texture reference bound to a CUDA array supporting texture gather| 32768 x 32768  
Maximum width, height, and number of layers for a 2D layered texture reference| 32768 x 32768 x 2048	  
Maximum width, height and depth for a 3D texture reference bound to linear memory or a CUDA array| 16384^3	  
Maximum width (and height) for a cubemap texture reference| 32768  
jMaximum width (and height) and number of layers for a cubemap layered texture reference| 32768 × 2046  
Maximum number of textures that can be bound to a kernel| 256  
Maximum width for a 1D surface reference bound to a CUDA array	| 32768  
Maximum width and number of layers for a 1D layered surface reference| 32768 x 2048	  
Maximum width and height for a 2D surface reference bound to a CUDA array| 131072 x 65536  
Maximum width, height, and number of layers for a 2D layered surface reference| 32768 × 32768 × 2048	  
Maximum width, height, and depth for a 3D surface reference bound to a CUDA array| 16384 × 16384 × 16384	  
Maximum width (and height) for a cubemap surface reference bound to a CUDA array| 32768  
Maximum width and number of layers for a cubemap layered surface reference| 32768 × 2046	  
Maximum number of surfaces that can be bound to a kernel| 16  
Maximum number of instructions per kernel| 512 million  
Number of ALU lanes for integer and single-precision floating-point arithmetic operations| 128  
Number of special function units for single-precision floating-point transcendental functions| 32  
Number of texture filtering units for every texture address unit or render output unit (ROP)| 8  
Number of warp schedulers| 4  
Max number of instructions issued at once by a single scheduler| 1  
Number of tensor cores| N/A  
**Register allocation unit size**| 256
Register allocation granularity| warp
Shared Memory allocation unit size| 256
Warp allocation granularity| 4



## GeForce GTX 1080  
Pascal microarchitecture

|spec|value|
|---|---|
CUDA Capability Major/Minor version number|    6.1
Total amount of global memory|                 8120 MBytes (8513978368 bytes)
(20) Multiprocessors, (128) CUDA Cores/MP|     2560 CUDA Cores
GPU Max Clock rate|                            1835 MHz (1.84 GHz)
Memory Clock rate|                             5005 Mhz
Memory Bus Width|                              256-bit
L2 Cache Size|                                 2097152 bytes
Maximum Texture Dimension Size (x,y,z)|        1D=(131072), 2D=(131072, 65536), 3D=(16384, 16384, 16384)
Maximum Layered 1D Texture Size, (num) layers| 1D=(32768), 2048 layers
Maximum Layered 2D Texture Size, (num) layers| 2D=(32768, 32768), 2048 layers
Total amount of constant memory|               65536 bytes
Total amount of shared memory per block|       49152 bytes
Total number of registers available per block| 65536
Warp size|                                     32
Maximum number of threads per multiprocessor|  2048
Maximum number of threads per block|           1024
Max dimension size of a thread block (x,y,z)| (1024, 1024, 64)
Max dimension size of a grid size    (x,y,z)| (2147483647, 65535, 65535)
Maximum memory pitch|                          2147483647 bytes
Texture alignment|                             512 bytes
Concurrent copy and kernel execution|          Yes with 2 copy engine(s)
Run time limit on kernels|                     No
Integrated GPU sharing Host Memory|            No
Support host page-locked memory mapping|       Yes
Alignment requirement for Surfaces|            Yes
Device has ECC support|                        Disabled
Device supports Unified Addressing (UVA)|      Yes
Supports Cooperative Kernel Launch|            Yes
Supports MultiDevice Co-op Kernel Launch|      Yes



_ _ _ 
# Reference
[CUDA SPEC - wiki](https://en.wikipedia.org/wiki/CUDA#Version_features_and_specifications)  
CUDA Occupancy Calculator
