# Parallel computing labs

These labs are some workshops about parallel computing.

- [lab1](./lab1) introduces the platform used in the course.
- [lab2](./lab2) introduces the usage of MPI.
- [lab3](./lab3) introduces the GPU cluster used in the course.
- [lab4(nvidia_course)](./lab4(nvidia_course)) is a workshop about CUDA with C/C++.
- [lab5(nvidia_course)](./lab5(nvidia_course)) is a workshop about CUDA with Python.
